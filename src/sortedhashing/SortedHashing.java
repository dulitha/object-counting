/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sortedhashing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Duli-chan
 */
public class SortedHashing {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List l = new ArrayList();
        for (int i = 0; i < 10; i++) {
            Stock s = new Stock();
            s.setIdStock(i);
            l.add(i);
            if (i == 5) {
                l.add(i);
            }
            if (i == 8) {
                l.remove(i);
            }
        }
        List<Object> objectCount = objectCount(l);
        for (int i = 0; i < objectCount.size(); i++) {
            Stock object = (Stock) objectCount.get(i);
            System.out.println(object.getIdStock());
        }
    }

    public static List<Object> objectCount(List a) {
        HashMap<Integer, Object> hashXcounter = new HashMap<Integer, Object>();
        Map<Integer, Object> hashXObject = new HashMap<Integer, Object>();

        Map<Object, Integer> ordered = new HashMap<Object, Integer>();
        for (int i = 0; i < a.size(); i++) {
            Object object = a.get(i);
            if (hashXcounter.get(object.hashCode()) != null) {
                hashXcounter.put(object.hashCode(), hashXcounter.get(object.hashCode()));
            } else {
                hashXcounter.put(object.hashCode(), 0);
                hashXObject.put(object.hashCode(), object);
            }
        }
        Collection l = hashXcounter.values();

        for (Map.Entry e : hashXcounter.entrySet()) {
            System.out.println("Testing");
            Integer key = (Integer) e.getKey();
            Integer max = (Integer) Collections.max(l);
            ordered.put(hashXObject.get(key), max);
            l.remove(max);
        }

        return ((List) ordered.values());
    }
}
